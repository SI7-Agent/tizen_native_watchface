#ifndef DATA_H_
#define DATA_H_

#include <Elementary.h>

#include <app_i18n.h>
#include <app_manager.h>
#include <device/battery.h>
#include <device/callback.h>
#include <dirent.h>
#include <dlog.h>
#include <package_manager.h>
#include <privacy_privilege_manager.h>
#include <sensor.h>
#include <stdlib.h>

#include <watch_app.h>
#include <watch_app_efl.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "digital-neon"

#define SENSORS_SIZE 2
#define PRIVILEGIES_SIZE 4
#define ERRORS_SIZE 3
#define POPUP_FONT_SIZE 30

enum touch_zones
{
	BG_TOUCH_ZONE = 0,
	APPLAUNCHER_TOUCH_ZONE,
	DATE_TOUCH_ZONE,
	DAY_TOUCH_ZONE,
	TIME_TOUCH_ZONE,
	STEP_TOUCH_ZONE,
	HRM_TOUCH_ZONE
};

typedef struct sensor
{
	sensor_h sensor;
	sensor_listener_h listener;
} sensor_s;

typedef struct permission_handler
{
	void* popup;
	int idx;
} permission_handler_s;

typedef struct popup
{
	Evas_Object* popup;
	Evas_Object* label;
} popup_s;

typedef struct appdata
{
	Evas_Object *win;
	Evas_Object *conform;
	Evas_Object *edje;
	Evas_Object *layout;
	Evas_Object *bg;
	popup_s *popup;
	Evas_Object *appmanager_touch_zone;
	Evas_Object *date_touch_zone;
	Evas_Object *time_touch_zone;
	Evas_Object *day_touch_zone;
	Evas_Object *step_touch_zone;
	Evas_Object *hrm_touch_zone;
	sensor_s sensors[SENSORS_SIZE];
	int access[PRIVILEGIES_SIZE];
} appdata_s;

typedef struct timer
{
	struct timeval time_start;
	struct timeval time_end;
} timer_s;

typedef struct sensor_opt
{
	sensor_type_e id;
	sensor_option_e power_mode;
}sensor_opt_s;

typedef struct date
{
	int hour;
	int minute;
	int second;
	int day;
	int week_day;
	int month;
} date_s;

typedef struct battery_status
{
	bool charging;
	int power;
} battery_status_s;

static sensor_opt_s sensor_ids[SENSORS_SIZE] = {
		{
			.id = SENSOR_HUMAN_PEDOMETER,
			.power_mode = SENSOR_OPTION_ON_IN_SCREEN_OFF
		},
		{
			.id = SENSOR_HRM,
			.power_mode = SENSOR_OPTION_DEFAULT
		}
};
static const char* privilegies[PRIVILEGIES_SIZE] = {
		"http://tizen.org/privilege/healthinfo",
		"http://tizen.org/privilege/appmanager.launch",
		"http://tizen.org/privilege/alarm.set",
		"http://tizen.org/privilege/packagemanager.info"
};

static int sensor_start_values[SENSORS_SIZE] = {0,};

static timer_s gesture_timer;

#define EDJE_FILE_PATH "edje/watch-structure.edj"
#define DOUBLE_TAP_DELAY 250000 //microsecs
#define TEXT_BUF_SIZE 256

void _sensor_callback4(sensor_h sensor, sensor_event_s *event, void *user_data);
void _sensor_callback5(sensor_h sensor, sensor_event_s events[], int events_count, void *user_data);
char* int_to_char(int num, int mode);
void data_get_resource_path(const char* file_in, char* file_path_out, int file_path_max);
void list_dir(const char* dir_path);
sensor_s create_sensor(sensor_opt_s options, void* ad);
void init_sensors_start_values(appdata_s* ad);
void set_sensors(appdata_s* ad);
void set_data(appdata_s* ad);
date_s get_date(watch_time_h watch_time);
battery_status_s get_battery_status();
int define_touch_zone(Evas_Point touch_point);
int launch_app(const char* package_name);
const char* get_restriction(int idx);
char* create_popup_message(int idx, const char* optional);

#endif /* DATA_H_ */
