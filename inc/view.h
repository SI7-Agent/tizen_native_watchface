#ifndef VIEW_H_
#define VIEW_H_

#include <data.h>
#include <stdbool.h>

enum message_ids {
		BATT_CHARGE = 1,
		BATT_HIGH,
		BATT_EMPTY,
		BATT_MIDDLE,
		BATT_FULL,
		BATT_AOD,
		MOVE_SECONDS
};

static const char* month_keys[12] = {
		"JANUARY_KEY",
		"FEBRUARY_KEY",
		"MARCH_KEY",
		"APRIL_KEY",
		"MAY_KEY",
		"JUNE_KEY",
		"JULY_KEY",
		"AUGUST_KEY",
		"SEPTEMBER_KEY",
		"OCTOBER_KEY",
		"NOVEMBER_KEY",
		"DECEMBER_KEY"
};

static const char* weekday_keys[7] = {
		"SUNDAY_KEY",
		"MONDAY_KEY",
		"TUESDAY_KEY",
		"WEDNESDAY_KEY",
		"THURSDAY_KEY",
		"FRIDAY_KEY",
		"SATURDAY_KEY"
};

void set_second_mover(Evas_Object* edje, int second);
void color_percents(Evas_Object* edje, battery_status_s status, bool aod);
void setup_layout(Evas_Object* layout);
void _screen_clicked(void* data, Evas_Object* obj, void* event_info);
void set_bg(appdata_s* ad, int width, int height);
void set_layout(appdata_s* ad);
void _timeout_cb(void *data, Evas_Object *obj, void *event_info);
void set_conform(appdata_s* ad);
Eina_Bool set_view(appdata_s* ad, int width, int height);
void view_date(Evas_Object* layout, date_s date);
void localizate(Evas_Object* parent, const char* part_name, const char* text_key);
void set_popup(appdata_s* ad);
void show_popup(popup_s *popup, int idx, const char* optional);
const char* get_restriction(int idx);
void app_check_and_request_permission(appdata_s* ad);
void ppm_request_response_handler(ppm_call_cause_e cause, ppm_request_result_e result, const char *privilege, void *user_data);
Evas_Object* create_touch_zone(Evas_Object* edje, Evas_Point start_point, int width, int height);
void set_touch_zones(appdata_s* ad);

#endif /* VIEW_H_ */
