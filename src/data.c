#include <data.h>

void _sensor_callback4(sensor_h sensor, sensor_event_s *event, void *user_data)
{
	sensor_type_e type;
	sensor_get_type(sensor, &type);

	if (type == SENSOR_HRM)
	{
		int v = (int)event->values[0];
		if (v != 0)
		{
			elm_object_part_text_set(((appdata_s*)user_data)->layout, "hpm", int_to_char(v, 0));
			sensor_listener_stop(((appdata_s*)user_data)->sensors[1].listener);
		}
	}
	else if (type == SENSOR_HUMAN_PEDOMETER)
	{
        int s = (int)event->values[0];
        elm_object_part_text_set(((appdata_s*)user_data)->layout, "steps", int_to_char(s-sensor_start_values[0], 0));
	}
}

void _sensor_callback5(sensor_h sensor, sensor_event_s events[], int events_count, void *user_data)
{
	sensor_type_e type;
	sensor_get_type(sensor, &type);

	if (type == SENSOR_HRM)
	{
		int v = (int)events[0].values[0];
		if (v != 0)
		{
			elm_object_part_text_set(((appdata_s*)user_data)->layout, "hpm", int_to_char(v, 0));
			sensor_listener_stop(((appdata_s*)user_data)->sensors[1].listener);
		}
	}
	else if (type == SENSOR_HUMAN_PEDOMETER)
	{
        int s = (int)events[0].values[0];
        elm_object_part_text_set(((appdata_s*)user_data)->layout, "steps", int_to_char(s-sensor_start_values[0], 0));
	}
}

char* int_to_char(int num, int mode)
{
	char* buf = (char*)calloc(12, sizeof(char));
	buf[0] = '-';
	int sign = (num>=0) ? 0 : 1;

	if (mode)
	{
		int module = abs(num);
		int start = (module<10) ? 1 : 0;

		buf[sign] = '0';
		sprintf(buf+sign+start, "%d", module);
	}
	else
	{
		sprintf(buf, "%d", num);
	}

	return buf;
}

void data_get_resource_path(const char* file_in, char* file_path_out, int file_path_max)
{
	char* res_path = app_get_resource_path();
	if (res_path)
	{
		snprintf(file_path_out, file_path_max, "%s%s", res_path, file_in);
 	 	free(res_path);
 	}
}

void list_dir(const char* dir_path)
{
    DIR* dir;
    struct dirent* de;

    dir = opendir(dir_path);
    while(dir)
    {
        de = readdir(dir);
        if (!de) break;
        dlog_print(DLOG_ERROR, LOG_TAG, "PATH CHECK = %i %s", de->d_type, de->d_name);
    }
    closedir(dir);
}

sensor_s create_sensor(sensor_opt_s options, void* ad)
{
	sensor_s sen = {0,};

    sensor_h sensor;
    sensor_listener_h listener;

    sensor_get_default_sensor(options.id, &sensor);
    sensor_create_listener(sensor, &listener);
    sensor_listener_set_option(listener, options.power_mode);
    sensor_listener_set_event_cb(listener, 0, _sensor_callback4, ad);
    sensor_listener_set_attribute_int(listener, SENSOR_ATTRIBUTE_PAUSE_POLICY, SENSOR_PAUSE_ON_POWERSAVE_MODE);

    sen.sensor = sensor;
    sen.listener = listener;

    return sen;
}

void set_sensors(appdata_s* ad)
{
	for (int i = 0; i < SENSORS_SIZE; i++)
	{
		ad->sensors[i] = create_sensor(sensor_ids[i], (void*)ad);
		sensor_listener_start(ad->sensors[i].listener);
	}
}

void init_sensors_start_values(appdata_s* ad)
{
	for (int i = 0; i < SENSORS_SIZE; i++)
	{
		sensor_event_s event;
		sensor_listener_read_data(ad->sensors[i].listener, &event);

		sensor_start_values[i] = (event.values[0] <= 0) ? 0 : (int)event.values[0];
	}
}

void set_data(appdata_s* ad)
{
	if (ad->access[0])
	{
		set_sensors(ad);
		init_sensors_start_values(ad);
	}
}

date_s get_date(watch_time_h watch_time)
{
	date_s current_date = {0,};

	int hour24, minute, second, day, day_of_week, month;

	watch_time_get_hour24(watch_time, &hour24);
	watch_time_get_minute(watch_time, &minute);
	watch_time_get_second(watch_time, &second);
	watch_time_get_day(watch_time, &day);
	watch_time_get_day_of_week(watch_time, &day_of_week);
	watch_time_get_month(watch_time, &month);

	current_date.hour = hour24;
	current_date.minute = minute;
	current_date.second = second;
	current_date.day = day;
	current_date.week_day = day_of_week;
	current_date.month = month;

	return current_date;
}

battery_status_s get_battery_status()
{
	battery_status_s bs;

	int battery;
	bool charging;

	device_battery_get_percent(&battery);
	device_battery_is_charging(&charging);

	bs.charging = charging;
	bs.power = battery;

	return bs;
}

int define_touch_zone(Evas_Point touch_point)
{
	int zone = BG_TOUCH_ZONE;

	if ((touch_point.x <= 205) && (touch_point.x >= 155))
		if ((touch_point.y <= 70) && (touch_point.y >= 20))
			zone = APPLAUNCHER_TOUCH_ZONE;

	if ((touch_point.x <= 290) && (touch_point.x >= 70))
		if ((touch_point.y <= 115) && (touch_point.y >= 80))
			zone = DATE_TOUCH_ZONE;

	if ((touch_point.x <= 340) && (touch_point.x >= 295))
		if ((touch_point.y <= 203) && (touch_point.y >= 158))
			zone = DAY_TOUCH_ZONE;

	if ((touch_point.x <= 292) && (touch_point.x >= 68))
		if ((touch_point.y <= 225) && (touch_point.y >= 125))
			zone = TIME_TOUCH_ZONE;

	if ((touch_point.x <= 174) && (touch_point.x >= 74))
		if ((touch_point.y <= 293) && (touch_point.y >= 248))
			zone = STEP_TOUCH_ZONE;

	if ((touch_point.x <= 285) && (touch_point.x >= 185))
		if ((touch_point.y <= 293) && (touch_point.y >= 248))
			zone = HRM_TOUCH_ZONE;

	return zone;
}

int launch_app(const char* package_name)
{
	int err;
	app_control_h app_control;

	app_control_create(&app_control);
	app_control_set_operation(app_control, APP_CONTROL_OPERATION_DEFAULT);
	app_control_set_app_id(app_control, package_name);

	err = app_control_send_launch_request(app_control, NULL, NULL);
	app_control_destroy(app_control);

	return err;
}

char* create_popup_message(int idx, const char* optional)
{
	char* message = (char*)calloc(1024, sizeof(char));

	if (idx < PRIVILEGIES_SIZE)
		snprintf(message, 1023, get_restriction(idx), POPUP_FONT_SIZE);
	else
		snprintf(message, 1023, get_restriction(idx), POPUP_FONT_SIZE, optional);

	return message;
}

const char* get_restriction(int idx)
{
	const char* restrictions[PRIVILEGIES_SIZE + ERRORS_SIZE] = {
			i18n_get_text("SENSORS_RESTRICTION_KEY"),
			i18n_get_text("LAUNCHER_RESTRICTION_KEY"),
			i18n_get_text("ALARM_RESTRICTION_KEY"),
			i18n_get_text("PACKAGE_MANAGER_RESTRICTION_KEY"),
			i18n_get_text("APP_NOT_FOUND_KEY"),
			i18n_get_text("APP_ERROR_KEY"),
			i18n_get_text("REBOOT_WATCHFACE_KEY")
	};

	return restrictions[idx];
}
