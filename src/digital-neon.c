#include <digital-neon.h>

static void
update_watch(appdata_s* ad, watch_time_h watch_time, bool ambient)
{
	date_s date = get_date(watch_time);
	battery_status_s battery = get_battery_status();

	if (ad->access[0])
	{
		if (date.hour == 0)
			if((date.minute == 0) && (date.second == 0))
				init_sensors_start_values(ad);

		if (date.hour == 2)
			if((date.minute == 0) && (date.second == 0))
				sensor_listener_start(ad->sensors[1].listener);
	}

	if (!ambient)
	{
		set_second_mover(ad->edje, date.second);
	}

	view_date(ad->layout, date);
	color_percents(ad->edje, battery, ambient);
}


static bool
app_create(int width, int height, void *data)
{
	appdata_s* ad = data;
	for (int i = 0; i < PRIVILEGIES_SIZE; i++)
		ad->access[i] = 0;

	if (set_view(ad, width, height) == EINA_FALSE)
		return false;

	app_check_and_request_permission(ad);
	set_data(ad);

	return true;
}

static void
app_control(app_control_h app_control, void* data)
{
	/* Handle the launch request. */
}

static void
app_pause(void* data)
{
	/* Take necessary actions when application becomes invisible. */
}

static void
app_resume(void* data)
{
	/* Take necessary actions when application becomes visible. */
}

static void
app_terminate(void* data)
{
	/* Release all resources. */
}

static void
app_time_tick(watch_time_h watch_time, void* data)
{
	/* Called at each second while your app is visible. Update watch UI. */
	appdata_s* ad = data;
	update_watch(ad, watch_time, false);
}

static void
app_ambient_tick(watch_time_h watch_time, void* data)
{
	/* Called at each minute while the device is in ambient mode. Update watch UI. */
	appdata_s* ad = data;
	update_watch(ad, watch_time, true);
}

static void
app_ambient_changed(bool ambient_mode, void* data)
{
	if (ambient_mode)
		elm_object_signal_emit(((appdata_s*)data)->layout, "ambient,in", "");
	else
		elm_object_signal_emit(((appdata_s*)data)->layout, "ambient,out", "");

	/* Update your watch UI to conform to the ambient mode */
}

static void
watch_app_lang_changed(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_LANGUAGE_CHANGED*/
	char* locale = NULL;
	app_event_get_language(event_info, &locale);
	elm_language_set(locale);
	free(locale);
	return;
}

static void
watch_app_region_changed(app_event_info_h event_info, void* user_data)
{
	/*APP_EVENT_REGION_FORMAT_CHANGED*/
}

int
main(int argc, char* argv[])
{
	appdata_s ad = {0,};
	int ret = 0;

	watch_app_lifecycle_callback_s event_callback = {0,};
	app_event_handler_h handlers[5] = {NULL, };

	event_callback.create = app_create;
	event_callback.terminate = app_terminate;
	event_callback.pause = app_pause;
	event_callback.resume = app_resume;
	event_callback.app_control = app_control;
	event_callback.time_tick = app_time_tick;
	event_callback.ambient_tick = app_ambient_tick;
	event_callback.ambient_changed = app_ambient_changed;

	watch_app_add_event_handler(&handlers[APP_EVENT_LANGUAGE_CHANGED],
		APP_EVENT_LANGUAGE_CHANGED, watch_app_lang_changed, &ad);
	watch_app_add_event_handler(&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
		APP_EVENT_REGION_FORMAT_CHANGED, watch_app_region_changed, &ad);

	ret = watch_app_main(argc, argv, &event_callback, &ad);
	if (ret != APP_ERROR_NONE)
		dlog_print(DLOG_ERROR, LOG_TAG, "watch_app_main() is failed. err = %d", ret);

	return ret;
}

