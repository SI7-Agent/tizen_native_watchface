#include <view.h>

void set_second_mover(Evas_Object* edje, int second)
{
	Edje_Message_Int* msg;
	msg = alloca(sizeof(Edje_Message_Int));
	msg->val = second;

	edje_object_message_send(edje, EDJE_MESSAGE_INT, MOVE_SECONDS, msg);
}

void color_percents(Evas_Object* edje, battery_status_s status, bool aod)
{
	Edje_Message_Int* msg;
	msg = alloca(sizeof(Edje_Message_Int));
	msg->val = status.power;

	if (aod)
		edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_AOD, msg);
	else
		if (status.charging)
			edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_CHARGE, msg);
		else if (status.power == 100)
			edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_FULL, msg);
		else
			switch((status.power/10)%10)
			{
			case 0:
			case 1:
			case 2:
				edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_EMPTY, msg);
				break;

			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_MIDDLE, msg);
				break;

			default:
				edje_object_message_send(edje, EDJE_MESSAGE_INT, BATT_HIGH, msg);
				break;
			}
}

void setup_layout(Evas_Object* layout)
{
	char edje_path[PATH_MAX];
	data_get_resource_path(EDJE_FILE_PATH, edje_path, sizeof(edje_path)-1);
	elm_layout_file_set(layout, edje_path, "digital-neon");
	evas_object_size_hint_weight_set(layout, 360, 360);
	evas_object_show(layout);
}

void _timeout_cb(void *data, Evas_Object *obj, void *event_info)
{
	evas_object_hide(obj);
}

void _screen_clicked(void* data, Evas_Object* obj, void* event_info)
{
	if ((gesture_timer.time_start.tv_sec == 0) && (gesture_timer.time_start.tv_usec == 0))
	{
		//первое нажатие
		gettimeofday(&gesture_timer.time_start, NULL);
	}
	else
	{
		//нажатие второе
		gettimeofday(&gesture_timer.time_end, NULL);
		long int delay = (gesture_timer.time_end.tv_sec - gesture_timer.time_start.tv_sec) * 1000000 + gesture_timer.time_end.tv_usec - gesture_timer.time_start.tv_usec;
		if (delay > DOUBLE_TAP_DELAY)
		{
			//одиночное нажатие 2
			gesture_timer.time_start.tv_sec = gesture_timer.time_end.tv_sec;
			gesture_timer.time_start.tv_usec = gesture_timer.time_end.tv_usec;
			gesture_timer.time_end.tv_sec = 0;
			gesture_timer.time_end.tv_usec = 0;
		}
		else
		{
			//двойное нажатие
			gesture_timer.time_start.tv_sec = 0;
			gesture_timer.time_start.tv_usec = 0;
			gesture_timer.time_end.tv_sec = 0;
			gesture_timer.time_end.tv_usec = 0;

			appdata_s* ad = (appdata_s*)data;
			Evas_Point touch_point;
			evas_pointer_canvas_xy_get(obj, &touch_point.x, &touch_point.y);

			int zone = define_touch_zone(touch_point);

			int ret;
			bool app = false;
			char* package_name = calloc(TEXT_BUF_SIZE, sizeof(char));

			switch(zone)
			{
			case APPLAUNCHER_TOUCH_ZONE:
				snprintf(package_name, TEXT_BUF_SIZE - 1, "com.samsung.w-taskmanager");
				app = true;
				break;

			case DATE_TOUCH_ZONE:
			case DAY_TOUCH_ZONE:
			{
				snprintf(package_name, TEXT_BUF_SIZE - 1, "com.samsung.w-calendar2");
				app = true;
				break;
			}

			case TIME_TOUCH_ZONE:
				elm_object_signal_emit(ad->layout, "change,font", "");
				break;

			case STEP_TOUCH_ZONE:
			case HRM_TOUCH_ZONE:
				snprintf(package_name, TEXT_BUF_SIZE - 1, "com.samsung.shealth");
				app = true;
				break;

			default:
				elm_object_signal_emit(ad->layout, "change,theme", "");
				break;
			}

			if (app)
				if ((ret = launch_app(package_name)) != APP_CONTROL_ERROR_NONE)
					if (ret == APP_CONTROL_ERROR_APP_NOT_FOUND)
						show_popup(ad->popup, PRIVILEGIES_SIZE, package_name);
					else
						show_popup(ad->popup, PRIVILEGIES_SIZE + 1, package_name);
		}
	}
}

void set_bg(appdata_s* ad, int width, int height)
{
	elm_win_autodel_set(ad->win, EINA_TRUE);
	Evas* evas = evas_object_evas_get(ad->win);

	ad->bg = evas_object_rectangle_add(evas);
	evas_object_color_set(ad->bg, 0, 0, 0, 255);
	evas_object_resize(ad->bg, width, height);
	evas_object_move(ad->bg, 0, 0);

	evas_object_stack_below(ad->bg, ad->win);
	evas_object_show(ad->bg);
}

void set_layout(appdata_s* ad)
{
	ad->layout = elm_layout_add(ad->conform);
	elm_object_content_set(ad->conform, ad->layout);
	setup_layout(ad->layout);
	ad->edje = elm_layout_edje_get(ad->layout);
}

void set_conform(appdata_s* ad)
{
	ad->conform = elm_conformant_add(ad->win);
	evas_object_size_hint_weight_set(ad->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(ad->win, ad->conform);
	evas_object_show(ad->conform);
}

Eina_Bool set_view(appdata_s* ad, int width, int height)
{
	int ret = watch_app_get_elm_win(&ad->win);
	if (ret != APP_ERROR_NONE)
	{
		dlog_print(DLOG_ERROR, LOG_TAG, "failed to get window. err = %d", ret);
		return EINA_FALSE;
	}

	evas_object_resize(ad->win, width, height);

	set_conform(ad);
	set_layout(ad);
	set_popup(ad);
	set_bg(ad, width, height);
	set_touch_zones(ad);

	evas_object_event_callback_add(ad->layout, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);
	evas_object_show(ad->win);

	return EINA_TRUE;
}

void view_date(Evas_Object* layout, date_s date)
{
	char* hours = int_to_char(date.hour, 1);
	char* minutes = int_to_char(date.minute, 1);
	char* seconds = int_to_char(date.second, 1);
	char* days = int_to_char(date.day, 1);

	elm_object_part_text_set(layout, "hours-place", hours);
	elm_object_part_text_set(layout, "minutes-place", minutes);
	elm_object_part_text_set(layout, "seconds-place", seconds);
	elm_object_part_text_set(layout, "day", days);

	const char* weekday = weekday_keys[date.week_day-1];
	const char* month = month_keys[date.month-1];

	localizate(layout, "month", month);
	localizate(layout, "weekday", weekday);

	free(hours);
	free(minutes);
	free(seconds);
	free(days);
}

void localizate(Evas_Object* parent, const char* part_name, const char* text_key)
{
	char* localizated = i18n_get_text(text_key);
	elm_object_part_text_set(parent, part_name, localizated);
}

void set_popup(appdata_s* ad)
{
	Evas_Object *popup, *content;
	popup_s* object = (popup_s*)malloc(sizeof(popup_s));

	popup = elm_popup_add(ad->edje);
	content = elm_label_add(popup);
	evas_object_name_set(content, "label");

	elm_object_style_set(popup, "circle");
	elm_popup_timeout_set(popup, 5.0);
	elm_popup_align_set(popup, 0.5, 0.5);
	evas_object_smart_callback_add(popup, "timeout", _timeout_cb, NULL);

	elm_object_content_set(popup, content);

	object->label = content;
	object->popup = popup;
	ad->popup = object;
}

void show_popup(popup_s *popup, int idx, const char* optional)
{
	char* msg = create_popup_message(idx, optional);
	elm_object_text_set(popup->label, msg);
	evas_object_show(popup->popup);
	free(msg);
}

void app_check_and_request_permission(appdata_s* ad)
{
	for (int i = 0; i < PRIVILEGIES_SIZE; i++)
	{
	    ppm_check_result_e result;
	    ppm_check_permission(privilegies[i], &result);

	    if (result == PRIVACY_PRIVILEGE_MANAGER_CHECK_RESULT_ASK)
	    {
	    	permission_handler_s* data = (permission_handler_s*)malloc(sizeof(permission_handler_s));
	    	data->popup = (void*)ad;
	    	data->idx = i;

	    	ppm_request_permission(privilegies[i], ppm_request_response_handler, (void*)data);
	    }
	    else if (result == PRIVACY_PRIVILEGE_MANAGER_CHECK_RESULT_DENY)
	    	show_popup(ad->popup, i, NULL);
	    else
	    	ad->access[i] = 1;
	}
}

void ppm_request_response_handler(ppm_call_cause_e cause, ppm_request_result_e result, const char *privilege, void *user_data)
{
	if(cause == PRIVACY_PRIVILEGE_MANAGER_CALL_CAUSE_ANSWER)
    {
    	permission_handler_s* data = (permission_handler_s*)user_data;
        if(result != PRIVACY_PRIVILEGE_MANAGER_REQUEST_RESULT_ALLOW_FOREVER)
        	show_popup(((appdata_s*)data->popup)->popup, data->idx, NULL);
        else
        {
        	((appdata_s*)data->popup)->access[data->idx] = 1;
        	show_popup(((appdata_s*)data->popup)->popup, PRIVILEGIES_SIZE + 2, NULL);
        }

        free(user_data);
    }
}

Evas_Object* create_touch_zone(Evas_Object* edje, Evas_Point start_point, int width, int height)
{
	Evas* evas = evas_object_evas_get(edje);

	Evas_Object* touch_zone = evas_object_rectangle_add(evas);
	evas_object_color_set(touch_zone, 0, 0, 0, 0);
	evas_object_resize(touch_zone, width, height);
	evas_object_move(touch_zone, start_point.x, start_point.y);

	evas_object_stack_above(touch_zone, edje);
	evas_object_show(touch_zone);

	return touch_zone;
}

void set_touch_zones(appdata_s* ad)
{
	Evas_Point setting_point = {.x=0, .y=0};

	//create appmanager_touch_zone
	setting_point.x = 155;
	setting_point.y = 20;
	ad->appmanager_touch_zone = create_touch_zone(ad->layout, setting_point, 50, 50);
	evas_object_event_callback_add(ad->appmanager_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);

	//create date_touch_zone
	setting_point.x = 70;
	setting_point.y = 80;
	ad->date_touch_zone = create_touch_zone(ad->edje, setting_point, 220, 35);
	evas_object_event_callback_add(ad->date_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);

	//create time_touch_zone
	setting_point.x = 68;
	setting_point.y = 125;
	ad->time_touch_zone = create_touch_zone(ad->edje, setting_point, 224, 100);
	evas_object_event_callback_add(ad->time_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);

	//create day_touch_zone
	setting_point.x = 295;
	setting_point.y = 158;
	ad->day_touch_zone = create_touch_zone(ad->edje, setting_point, 45, 45);
	evas_object_event_callback_add(ad->day_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);

	//create step_touch_zone
	setting_point.x = 74;
	setting_point.y = 248;
	ad->step_touch_zone = create_touch_zone(ad->edje, setting_point, 100, 45);
	evas_object_event_callback_add(ad->step_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);

	//create hrm_touch_zone
	setting_point.x = 185;
	setting_point.y = 248;
	ad->hrm_touch_zone = create_touch_zone(ad->edje, setting_point, 100, 45);
	evas_object_event_callback_add(ad->hrm_touch_zone, EVAS_CALLBACK_MOUSE_DOWN, _screen_clicked, ad);
}
